const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const winpthreads_dep = b.dependency("winpthreads", .{});
    const lib = b.addStaticLibrary(.{
        .name = "winpthreads",
        .target = target,
        .optimize = optimize,
    });
    lib.linkLibC();
    if (optimize == .Debug) {
        lib.defineCMacro("WINPTHREAD_DBG", "1");
    }
    lib.defineCMacro("IN_WINPTHREAD", "");
    lib.defineCMacro("WIN32_LEAN_AND_MEAN", "");
    lib.defineCMacro("__USE_MINGW_ANSI_STDIO", "0");
    inline for (winpthreads_src_files) |src| {
        lib.addCSourceFile(.{
            .file = winpthreads_dep.path(prefix ++ src),
            .flags = &.{},
        });
    }
    inline for (winpthreads_inc_paths) |inc| {
        const path = prefix ++ inc;
        if (std.mem.eql(u8, inc, "include")) {
            const raw = winpthreads_dep.path(path).getPath(b);
            lib.installHeadersDirectory(raw, "");
        }
        lib.addIncludePath(winpthreads_dep.path(path));
    }
    b.installArtifact(lib);

    const main_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    main_tests.linkLibrary(lib);
    const run_main_tests = b.addRunArtifact(main_tests);
    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&run_main_tests.step);
}

const prefix = "mingw-w64-libraries/winpthreads/";
const winpthreads_inc_paths = [_][]const u8{
    "src",
    "include",
};

const winpthreads_src_files = [_][]const u8{
    "src/barrier.c",
    "src/cond.c",
    "src/misc.c",
    "src/mutex.c",
    "src/rwlock.c",
    "src/spinlock.c",
    "src/thread.c",
    "src/ref.c",
    "src/sem.c",
    "src/sched.c",
    "src/clock.c",
    "src/nanosleep.c",
    //"src/libgcc/dll_dependency.S",
    "src/libgcc/dll_math.c",
};
