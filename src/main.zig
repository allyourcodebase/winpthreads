const std = @import("std");
const testing = std.testing;
const c = @cImport(@cInclude("pthread.h"));

test "threads" {
    const Param = struct {
        id: usize,
        n_threads: usize,
    };
    const cb = struct {
        pub fn hello(arg: ?*anyopaque) callconv(.C) ?*anyopaque {
            var p: *Param = @ptrCast(@alignCast(arg));
            std.debug.assert(p.id < p.n_threads);
            c.pthread_exit(null);
            unreachable;
        }
    };
    var pthread_custom_attr: c.pthread_attr_t = undefined;
    var ret = c.pthread_attr_init(&pthread_custom_attr);
    std.debug.assert(ret == 0); // check return value
    const n_threads = try std.Thread.getCpuCount();
    var p = try testing.allocator.alloc(Param, n_threads);
    defer testing.allocator.free(p);
    var threads = try testing.allocator.alloc(c.pthread_t, n_threads);
    defer testing.allocator.free(threads);
    for (threads, 0..) |*t, i| {
        p[i] = .{ .id = i, .n_threads = n_threads };
        _ = c.pthread_create(t, &pthread_custom_attr, &cb.hello, &p[i]);
    }
    for (threads) |t| _ = c.pthread_join(t, null);
    c.pthread_exit(null);
}
